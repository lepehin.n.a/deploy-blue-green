FROM nginx:1.18.0-alpine

MAINTAINER Nik Lepekhin "lepehin.na@yandex.ru"

COPY build/ /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/
